
let num1 = +prompt("Enter first number");
let num2 = +prompt("Enter second number");
let operation = prompt("What do? (+, -, *, /)");

const calcNum = function (num1, num2, operation) {
    while (isNaN(num1) || isNaN(num2) || num1 == null || num2 == null || operation == null) {
        alert("Данные введены неправильно.");
        num1 = +prompt("Enter first number");
        num2 = +prompt("Enter second number");
        operation = prompt("What do? (+, -, *, /)");
    }

        if (operation === '+') {
            return num1 + num2;
        }
        else if (operation === '-') {
            return num1 - num2;
        }
        else if (operation === '*') {
            return num1 * num2;
        }
        else if (operation === '/') {
            return num1 / num2;
        } else {
            alert ("Введена неправильная операция")
    }
}
console.log(calcNum(num1, num2, operation));