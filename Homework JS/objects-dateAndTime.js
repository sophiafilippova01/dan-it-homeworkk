// let now = new Date();
// alert(now);

// let Jan01_1970 = new Date(0);
// alert(Jan01_1970);
//
// let Jan02_1970 = new Date(24 * 3600 * 1000);
// alert(Jan02_1970);

// let Dec_1969 = new Date(-24 * 3600 * 1000);
// alert(Dec_1969)

// let date = new Date('2017-01-26');
// alert(date);

// let date = new Date(2011, 0, 1, 0, 0, 0, 0);
// ИЛИ
// let date = new Date(2011, 0, 1);
// alert(date);

// let date = new Date(2011, 0, 1, 5, 20, 34, 567);
// alert(date);

// let date = new Date();
// alert(date.getHours());
// alert(date.getUTCHours());
// alert(date.getTimezoneOffset())

// let today = new Date();
// today.setHours(0);
// alert(today);
// today.setHours(0, 0, 0, 0);
// alert(today);

// let date = new Date(2013, 0, 32);
// alert(date);

// let date = new Date(2016, 1, 28);
// date.setDate(date.getDate() + 2);
// alert(date);

// let date = new Date();
// date.setSeconds(date.getSeconds() + 70);
// alert(date);
//
// let date = new Date(2016, 0, 2);
// date.setDate(1);
// alert(date);
//
// date.setDate(0);
// alert(date);

// let date = new Date();
// alert(+date);

// let start = new Date();
// for (let i = 0; i < 100000; i++) {
//     let doSomething = i * i *i;
// }
// let end = new Date();
// alert(`Цикл отработал ${end - start} миллисекунд`);

// let start = Date.now();
// for (let i = 0; i < 100000; i++) {
//     let doSomething = i * i *i;
// }
// let end = Date.now();
// alert(`Цикл отработал ${end - start} миллисекунд`);

// function diffSubtract (date1, date2) {
//     return date2 - date1;
// }
//
// function diffGetTime (date1, date2) {
//     return date2.getTime() - date1.getTime();
// }
//
// console.log(diffSubtract(28, 30));

// function diffSubtract(date1, date2) {
//     return date2 - date1;
// }
//
// function diffGetTime(date1, date2) {
//     return date2.getTime() - date1.getTime();
// }
//
// function bench(f) {
//     let date1 = new Date(0);
//     let date2 = new Date();
//
//     let start = Date.now();
//     for (let i = 0; i < 100000; i++) f(date1, date2);
//     return Date.now() - start;
// }
//
// alert( 'Время diffSubtract: ' + bench(diffSubtract) + 'мс' );
// alert( 'Время diffGetTime: ' + bench(diffGetTime) + 'мс' );

// let ms = Date.parse('2012-01-26T13:51:50.417-07:00');
// alert(ms);

let date = new Date( Date.parse('2012-01-26T13:51:50.417-07:00') );

alert(date);
