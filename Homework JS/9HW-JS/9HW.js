let target = ".tabs"
let elemTabs = (typeof target === "string" ? document.querySelector(target) : target);
function hideTabs () {
    let removeTabs = document.querySelector(".tabs-pane-show"); // элемент с классом
    if (removeTabs != null) {
        removeTabs.classList.remove("tabs-pane-show");
    }

    let activeTitle = document.querySelector(".active")
    if(activeTitle != null) {
        activeTitle.classList.remove("active");
    }
}

function myTabs() { // создаем функцию
    elemTabs.addEventListener("click", function (e) { // добавляем обработчик событий на elemTabs
        hideTabs(); // вызываем фунцию, которая скрывает все неактивные вкладки и заголовки
    // e.target - целевой элемент
        let idSelector = e.target.getAttribute("href"); // ищем в целевом элементе аттрибут href
        let tab = document.querySelector(idSelector); // ищем в документе табы (соответсвующую вкладку)
        tab.classList.add("tabs-pane-show"); // добавляем класс для видимости вкладки
        e.target.classList.add("active"); // отмечаем целевой элемент как активный
    });
}
let x = myTabs();
console.log(x)