let validateDiv = document.createElement("div"); // create

const input = document.querySelector('#price'); // find

function checkNumberKey(key) {
    return (key >= '0' && key <= '9') || key == '-' || key == 'ArrowLeft' || key == 'ArrowRight' || key == 'Delete' || key == 'Backspace';
}

input.addEventListener('focus', () => {
    input.style.border = '2px solid greenyellow';
});

input.addEventListener('blur', () => {
    input.style.border = '1px solid gray';
    
    if (input.value < 0) {
        validateDiv.innerText = "Please enter correct price";
        input.after(validateDiv);

        validateDiv.classList.add("validateDiv");
        input.classList.add("validateInput");
        input.classList.remove("goodInput");
    }
    else if (input.value !== "") {
        let div = document.createElement("div");
        let button = document.createElement("button");
        div.classList.add("goodDiv");
        input.before(div);
        div.innerText = `Текущая цена: "${input.value}"`;
        div.prepend(button);
        button.innerText = 'X';
        validateDiv.innerText = "";
        button.classList.add("btnStyle");
        input.classList.add("goodInput");

        button.addEventListener("click", () => {
            div.remove();
            input.value = "";
        });
    }
    input.value = "";
});