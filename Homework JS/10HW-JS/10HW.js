
let pass1 = document.querySelector(".password1"); // нахожу строку первую ввода
let pass2 = document.querySelector(".password2"); // нахожу строку вторую ввода
let errorMessage = document.createElement("p"); // формируем тег для сообщения

let icons = document.querySelector(".icons-eyes");
let input = document.querySelector(".pass");

let icon = document.querySelector(".icon");
let iconSlash = document.querySelector(".icon-slash");
let IconConfirm = document.querySelector(".confirm");
let IconConfirmSlash = document.querySelector(".confirm-slash");

let iconsEyes = document.querySelector(".icons-eyes"); // нашла блок с иконками
let iconsConfirm = document.querySelector(".confirm-icons"); // второй блок с иконками (сравнение)

let btn = document.querySelector(".btn");

function ShowOrHideEyes(eyeBlock, pass, eyeIcon, slahsIcon) { // функция "показать/скрыть" пароль
    eyeBlock.addEventListener("click", function (event) { // повесила событие на этот блок
        console.log(event.target); // беру, а потом вывожу дочерние элементы блока с иконками
        if (pass.getAttribute("type") === "password") {
            pass.setAttribute("type", "text");
            eyeIcon.classList.toggle("hide");
            slahsIcon.classList.toggle("hide");
        } else {
            pass.setAttribute("type", "password");
            eyeIcon.classList.toggle("hide");
            slahsIcon.classList.toggle("hide");
        };
    });
}

ShowOrHideEyes(iconsEyes, pass1, icon, iconSlash);
ShowOrHideEyes(iconsConfirm, pass2, IconConfirm, IconConfirmSlash);

function prevent() {
    btn.addEventListener("click", function (event) {
        event.preventDefault();
    })   
}

function validate() {
    pass1.value = "";
    pass2.value = "";
}

function comparison() { // функция сравнения значений в строке ввода
    if ((pass1.value == pass2.value) && (pass1.value != '' || pass2.value != '')) { // если значения начинок строк ввода совпадают, то пропускаем пользователя
        errorMessage.innerHTML = "";
        validate();
        alert("You are welcome");
    }
    else if (pass1.value != pass2.value) { // если значения начинок не совпадают - делаем следующее:
        // prevent();
        validate();
        errorMessage.innerHTML = "Нужно ввести одинаковые значения"; // наполняем тег текстом о несоответствии паролей
        errorMessage.className = "errMessage"; // добавляем класс для нашего сообщения и делаем его красным
        pass2.after(errorMessage); // ставим после второй строки ввода сообщение о несоответствии паролей
    } else {
        // prevent();
        errorMessage.innerHTML = "";
        alert("Ошибка ввода"); // если ничего из вышеперечисленного не актуально, выводим окно с текстом об ошибке
        validate();
    }
}

btn.addEventListener("click", () => comparison()); // при клике на кнопку вызываеться функция сравнения значений паролей