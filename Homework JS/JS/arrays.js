// const a = [10, null, 2]; // создаем массив
// a.length; //длинна массива
// a[a.length - 1]; // обращаемся к последнему элементу массива
// a[0]; // получаем индекс массива
// a[10];
// const a = []; // создаем пустой массив
// a[0] = 10; // добавляем элемент в массив
// const b = new Array (10);
// const c = Array();
// const  a = [10,2,3,4,'hello',6,7,8,9,10]; // создаем массив
// a.length = 3; //обрезаем массив

// МЕТОДЫ В МАССИВАХ(функции)
// ПРОСТЫЕ
// добавление в конец
// const a = [10, 2, 3];
// const res = a.push(4, 5, 6); //добавляем в конец массива элемент. метод возвращает длину массива после того как был
// // добавлен новый элемент
// console.log(a);
// console.log(res); // тут возвращает

// добавление в начало
// const a = [10, 2, 3];
// const res = a.unshift(1); // добавляем в начало массива элемент, возвращает новую длину массива
// console.log(a);
// console.log(res); // тут возвращает

// добавление в конец массива єлемент, так чтобы он был виден вначале

// const a = [10, 2, 3];
// const newA = [5, ...a]; // добавляем в массив a элемент так, чтобы он был первым
// console.log(newA);
//
// let a = [10, 2, 3];
// a = [...a, 3, 4, 7] // в данном случае массив не мутирует

// const someOfArguments = (...params) => {}; // в данной функции усть params, что значит - в массив может прийти
// сколько угодно параметров

// удаление элементов, при этом массив меняется, а функции pop и shift возвращают то, что вырезали
// const a = [10, 2, 3];
// const  res = a.pop(); // удаляет последний элемент, не принимает аргументы
// // const res1 = a.shift(); // удаляет первый элемент
// console.log(res);
// console.log(a);

// /**
//  * Задача 1.
//  Написать функцию, которая принимает массив товаров, а возвращает общую сумму по всем товарам.
//  если общая сумма более 100 - то необходимо вернуть сумму со скидкой в 10 процентов
//  */
// const humanData = [
//     {
//         data: 'dresses',
//         count: 5,
//         onePrice: 4.99
//     },
//     {
//         data: 'skirts',
//         count: 3,
//         onePrice: 6.99
//     },
//     {
//         data: 'hats',
//         count: 12,
//         onePrice: 9.90
//     },
//     {
//         data: 'shoes',
//         count: 12,
//         onePrice: 15.59
//     }]
//
// const countProductSum = (collection) => {
//     const sum = collection.reduce((prev, curr) => {
//         return prev + curr.count * curr.onePrice;
//     }, 0);
//     if (sum > 100) {
//         return sum * 0.9;
//     }
//     return sum;
// }
// countProductSum(humanData);
//
// // Задача 2
// //преобразовать полученное значение в массив объектов где каждый объект имеет свойтсва
// //центр - объект координат
// //название места
// //положение - объект со значениями - адрес и категория
// //отсортировать данные по параметру longitude ( в объекте коориднат )
// //вывести данный массив в консоль
// //использовать map/reduce/sort
//
//
// const apiGeoToken = 'pk.eyJ1Ijoic2VyZ2lpcGF0b2toYSIsImEiOiJjazk0OHI1ZnUwMmxyM2hvYnh3c2o3MW0wIn0.ilbTzTis5UCw-HGlKsnQlg';
// const geoURL = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
// const adress = 'Kyiv';
// (async () => {
//     const response = await fetch(`${geoURL}${adress}.json?access_token=${apiGeoToken}&limit=20`);
//     const data = await response.json();
//     const features = data.features;
//     const res = features.map(function (a) {
//         console.log(a.center); // [30.5, 50.25]
//         return {
//             center: {
//                 lat: a.center[0],
//                 lon: a.center[1],
//             },
//             placeName: '',
//             location : {
//                 address: features.,
//                 category: location,
//             },
//         }
//
//     });
//     // console.log(features);
// // Your code here
// })();
//
// // const apiGeoToken = 'pk.eyJ1Ijoic2VyZ2lpcGF0b2toYSIsImEiOiJjazk0OHI1ZnUwMmxyM2hvYnh3c2o3MW0wIn0.ilbTzTis5UCw-HGlKsnQlg';
// // const geoURL = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
// // const adress = 'Kyiv';
// // (async () => {
// //     const response = await fetch(`${geoURL}${adress}.json?access_token=${apiGeoToken}&limit=20`);
// //     const data = await response.json();
// // // Your code here
// //     console.log(data);
// // // Your code here
// // })();
//
//
//
//
// // const sum = (arg) => {
// //     const product = [];
// //     const price = humanData.reduce((prevItem, currentItem, index, array) => prevItem + currentItem);
// //     return price;
// //
// //
// //     return allSum;
// // }
// // console.log((sum));
//
// // const stringToUpper = () => {
// //     const array = [];
// //     let str = prompt('Enter a string');
// //     while(str !==null) {
// //         array.push();
// //         str = prompt('Enter a string');
// //     }
// //
// //
// //     // array.forEach((elem, index, array) => {
// //     //     array[index] = elem.toUpperCase();
// //     // });