
let name = prompt('Enter your name');
let lastName = prompt('Enter your last name');
let birthday = prompt('Date your birthday(dd.mm.yyyy)');

const createNewUser = function (name, lastName, birthday) {
    const newUser = {
        name,
        lastName,
        birthday,
        getLogin() {
            return this.name[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge () {
            let year = this.birthday.slice(6);
            let month = this.birthday.slice(3, 5);
            let day = this.birthday.slice(0, 2);

            const newBirthday = new Date(year, month, day);
            let calcAge = new Date(Date.now() - newBirthday);
            const ageUser = calcAge.getFullYear() - 1970;
            return ageUser;

        },
        getPassword () {
            return this.name[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        },
    }
    return newUser;
}

const res = createNewUser(name, lastName, birthday);
res.getLogin();
res.getAge();
res.getPassword()
console.log(res);
console.log(res.getLogin())
console.log(res.getAge())
console.log(res.getPassword())