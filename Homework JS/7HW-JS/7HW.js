
let array = ["hello", "world", 23, "Kharkiv", null, "Lviv"];
let listItem = document.getElementById("list-item");

const createFromArrayToList = function (arr, parent) {
    let result = arr.map(function (item) {
        return `<li>${item}</li>`;
    });
    parent.insertAdjacentHTML('afterbegin', result);
}
console.log(createFromArrayToList(array, listItem));