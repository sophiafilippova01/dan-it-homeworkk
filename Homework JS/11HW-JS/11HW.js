
let btns = document.querySelectorAll(".btn");
document.addEventListener("keypress", function (event) {
    console.log(event.key); // получаем нашу клавишу
    for (let elem of btns) { // прохожусь циклом по массиву кнопок
        elem.setAttribute("data-btn", "");
        let elemText = elem.innerText;
        if (elemText === event.key) {
            elem.setAttribute("data-btn", "blue");
        }
    };
})